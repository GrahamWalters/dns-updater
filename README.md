# DNS Updater

### Overview

1. Fetch current IP
2. Fetch domain IP
3. If different:
    1. Update domain
    2. Send push notification

### Usage

```bash
./dns-updater -env .env
```
