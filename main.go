package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	cloudflare "github.com/cloudflare/cloudflare-go"
	"github.com/joho/godotenv"
)

func currentIP() (string, error) {
	url := fmt.Sprintf("https://ipinfo.io/json?token=%s", os.Getenv("IP_INFO_TOKEN"))

	client := http.Client{
		Timeout: time.Duration(5 * time.Second),
	}

	r, err := client.Get(url)
	if err != nil {
		return "", err
	}
	defer r.Body.Close()

	target := struct {
		IP string `json:"ip"`
	}{}

	return target.IP, json.NewDecoder(r.Body).Decode(&target)
}

func sendNotification(message string) error {
	url := fmt.Sprintf("https://maker.ifttt.com/trigger/ip-changed/with/key/%s",
		os.Getenv("IFTTT_KEY"))

	payload := struct {
		Region string `json:"value1"`
		IP     string `json:"value2"`
	}{
		Region: os.Getenv("REGION"),
		IP:     message,
	}

	data, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	client := http.Client{
		Timeout: time.Duration(5 * time.Second),
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func loadEnv() error {
	var envFile string
	flag.StringVar(&envFile, "env", "", "env file path")
	flag.Parse()

	if envFile != "" {
		return godotenv.Load(envFile)
	}
	return nil
}

func main() {
	err := loadEnv()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	ip, err := currentIP()
	if err != nil {
		log.Fatal(err)
	}

	api, err := cloudflare.New(os.Getenv("CF_API_KEY"), os.Getenv("CF_API_EMAIL"))
	if err != nil {
		log.Fatal(err)
	}
	r, err := api.DNSRecord(
		os.Getenv("CF_ZONE_ID"),
		os.Getenv("CF_DNS_ID"))
	if err != nil {
		log.Fatal(err)
	}

	if r.Content != ip {
		fmt.Printf("IP changed from %s to %s\n", r.Content, ip)

		err = api.UpdateDNSRecord(
			os.Getenv("CF_ZONE_ID"),
			os.Getenv("CF_DNS_ID"),
			cloudflare.DNSRecord{Content: ip})
		if err != nil {
			log.Fatal(err)
		}

		err = sendNotification(ip)
		if err != nil {
			log.Fatal(err)
		}
	}
}
