module gitlab.com/GrahamWalters/dns-updater

go 1.14

require (
	github.com/cloudflare/cloudflare-go v0.8.5
	github.com/joho/godotenv v1.3.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/time v0.0.0-20181108054448-85acf8d2951c // indirect
)
